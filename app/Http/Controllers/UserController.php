<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Services\User\GetUserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function get(Request $request, GetUserService $service): UserResource
    {
        $user = $service->run($request);

        return UserResource::make($user);
    }
}
