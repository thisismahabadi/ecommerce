<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Services\Auth\SignInService;
use App\Services\Auth\SignUpService;
use Illuminate\Http\JsonResponse;

final class AuthController extends Controller
{
    public function register(RegisterRequest $request, SignUpService $service): JsonResponse
    {
        $signUp = $service->run($request);

        return response()->json($signUp, 201);
    }

    public function login(LoginRequest $request, SignInService $service): JsonResponse
    {
        $signIn = $service->run($request);

        return response()->json($signIn);
    }
}
