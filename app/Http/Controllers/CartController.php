<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Http\Resources\CartResource;
use App\Services\CartItem\StoreCartService;

final class CartController extends Controller
{
    public function store(CartRequest $request, StoreCartService $service): CartResource
    {
        $cart = $service->run($request);

        return CartResource::make($cart);
    }
}
