<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Discount extends Model
{
    protected $fillable = [
        'user_id',
        'code',
        'description',
        'amount',
        'is_active',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function discounts(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
