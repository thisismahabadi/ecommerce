<?php

declare(strict_types=1);

namespace App\Services\User;

use App\Models\User;
use Illuminate\Http\Request;

final class GetUserService
{
    public function run(Request $request): User
    {
        return $request->user();
    }
}
