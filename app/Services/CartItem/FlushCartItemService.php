<?php

declare(strict_types=1);

namespace App\Services\CartItem;

use App\Models\CartItem;

final class FlushCartItemService
{
    public function run(int $userId): void
    {
        CartItem::query()
            ->where('user_id', $userId)
            ->delete();
    }
}
