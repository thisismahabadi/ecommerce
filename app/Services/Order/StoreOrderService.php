<?php

declare(strict_types=1);

namespace App\Services\Order;

use App\Mail\GenerateOrderDiscount;
use App\Models\Order;
use App\Repositories\CartItem\CartItemRepository;
use App\Repositories\Discount\DiscountRepository;
use App\Services\CartItem\FlushCartItemService;
use App\Services\Discount\StoreDiscountService;
use App\Services\OrderItem\StoreOrderItemService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Exception;

final class StoreOrderService
{
    private DiscountRepository $discountRepository;
    private CartItemRepository $cartItemRepository;
    private CalculateOrderService $calculateOrderService;
    private StoreDiscountService $discountService;
    private FlushCartItemService $flushCartItemService;
    private StoreOrderItemService $orderItemService;

    public function __construct(
        DiscountRepository $discountRepository,
        CartItemRepository $cartItemRepository,
        CalculateOrderService $calculateOrderService,
        StoreDiscountService $discountService,
        FlushCartItemService $flushCartItemService,
        StoreOrderItemService $orderItemService
    ) {
        $this->discountRepository = $discountRepository;
        $this->cartItemRepository = $cartItemRepository;
        $this->calculateOrderService = $calculateOrderService;
        $this->discountService = $discountService;
        $this->flushCartItemService = $flushCartItemService;
        $this->orderItemService = $orderItemService;
    }

    public function run(Request $request): Order
    {
        return DB::transaction(function () use ($request) {
            $order = Order::query()
                ->create([
                    'user_id' => $request->user()->id,
                    'status' => 'COMPLETED',
                ]);

            $cartItems = $this->cartItemRepository->getUserItems($request->user()->id);

            if (!$cartItems) {
                throw new Exception('Shopping cart is empty.', 400);
            }

            $this->orderItemService->run($cartItems, $order);

            $orderAmount = $this->calculateOrderService->run($order->id, $request->user()->id, $request->input('discount_code'));

            $order->payment()->create(['amount' => $orderAmount, 'status' => 'PAID']);

            if ($request->has('discount_code')) {
                $discountId = $this->discountRepository->getDiscountIdByCodeAndUser($request->user()->id, $request->input('discount_code'));

                if (!$discountId) {
                    throw new Exception('The discount is not belong to you or it is not active.', 400);
                }

                $order->update(['discount_id' => $discountId]);

                $order->discount()->update(['is_active' => false]);
            }

            $this->flushCartItemService->run($request->user()->id);

            $discount = $this->discountService->run($request->user()->id, $order->id);

            Mail::to($request->user())
                ->later(Carbon::now()->addMinutes(15), new GenerateOrderDiscount($discount));

            return $order;
        });
    }
}
