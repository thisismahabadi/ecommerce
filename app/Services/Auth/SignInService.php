<?php

declare(strict_types=1);

namespace App\Services\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

final class SignInService
{
    public function run(Request $request): string
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            return $request->user()->createToken('auth_token')->plainTextToken;
        }

        throw new Exception('Credentials are wrong!', 401);
    }
}
