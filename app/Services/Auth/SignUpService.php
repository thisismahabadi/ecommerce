<?php

declare(strict_types=1);

namespace App\Services\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Exception;

final class SignUpService
{
    public function run(Request $request): string
    {
        $user = User::query()
            ->create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'mobile_number' => $request->input('mobile_number'),
                'password' => Hash::make($request->input('password')),
            ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        if ($token) {
            return $token;
        }

        throw new Exception('Something went wrong with registering and creating token.', 401);
    }
}
