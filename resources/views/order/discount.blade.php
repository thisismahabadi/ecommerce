Congratulations,

You received a discount with amount of {{ $discount->amount }} euros.

You can use it for the next purchase with this code: {{ $discount->code }}
