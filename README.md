### This is a small e-commerce project written in Laravel.

Here you can see the architecture (pattern) used by back-end:

Before anything, you'll see the api routes.

`routes/api.php`

Available routes:

- User sign-up: /api/v1/user/register
- User sign-in: /api/v1/user/login

You need authentication (by API) for these routes:

- Review user: /api/v1/user
- Add products to shopping cart: /api/v1/carts
- Complete the order: /api/v1/order

#### Built-in Laravel packaged used for API authentication: Sanctum

Then we have separated controllers for these routes which can be found here:

`app/Http/Controllers`

AuthController, CartController, OrderController, and UserController.

The first thing you will see in the controllers is the request files for validation so each route is safe by checking some default rules before going to the code.

You can check the validations via this path:

`app/Http/Requests`

You also are seeing that the responses are returning out of Resource class which are located in:

`app/Http/Resources`

and Here you see the Service/Repository pattern, which basically remove any logic from controllers and connect you to a Service class.

So, the controller is in charge of passing data to a service and returning the response to user.

Well, Services located here:

`app/Services`

Services then receive the data and implement some logic and then whenever need it will use Repositories for fetching data from database.

Also, for error handling you can see throw exceptions.

Repositories living in the `app/Repositories`

You can see a lot of database (Eloquent) actions are happening in a Repository class, so we separated the database level too.

For the specific use case (Send a discount code of 5 euros to user after checkout) I used Laravel built-in Email feature that you can see it in `app/Services/Order/StoreOrderService.php` class.

Then, we have Models which in the `app/Models` which you can see the relationships and database related topics there.

Other than that the database schemas are in `database/migrations` and contains all create table stuffs.

Let's go to the `tests` folder then, which you will see the Feature tests there that include all of the APIs scenarios by testing them and code development (maintain) will be safer for future.

So you can easily run the test after migrations:

`php artisan migrate` (You need a database first)

`php artisan test`

and I personally prefer to use the language strictness syntaxes to make sure almost everything should go to the excepted way, and that's why you will not see too much PHPDoc in the code (Not Laravel generated ones). By this I mean for example I try to add function return types or parameter type to the actual code instead of having them as a comment.

Since, it was a 2-hour task, I decided not to make it so huge project, so I would like to address somethings here:

- Address could be a separated database table/relation from user.
- You can keep track of what discount has been used or what was the amount via Order, and Discount tables.
- I made the custom classes as final because they will not be extended.
- Every Service class is doing separated so I try to keep the SOLID concept in.
