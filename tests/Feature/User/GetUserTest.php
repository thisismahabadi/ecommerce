<?php

declare(strict_types=1);

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

final class GetUserTest extends TestCase
{
    use RefreshDatabase;

    public function test_unauthenticated(): void
    {
        $this->getJson('api/v1/user')
            ->assertUnauthorized()
            ->assertExactJson([
                'message' => 'Unauthenticated.',
            ]);
    }

    public function test_a_user(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $this->actingAs($user);

        $this->getJson('api/v1/user')
            ->assertOk()
            ->assertExactJson([
                'data' => [
                    'id' => $user->id,
                    'name' => 'Mohammad',
                    'address' => 'Utrecht',
                    'email' => 'mohammad.mahabadi@hotmail.com',
                ],
            ]);
    }
}
