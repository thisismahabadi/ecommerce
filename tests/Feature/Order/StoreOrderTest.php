<?php

declare(strict_types=1);

namespace Tests\Feature\Order;

use App\Models\CartItem;
use App\Models\Discount;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

final class StoreOrderTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated(): void
    {
        $this->postJson('api/v1/orders')
            ->assertUnauthorized()
            ->assertExactJson([
                'message' => 'Unauthenticated.',
            ]);
    }

    public function test_with_empty_shopping_cart(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/orders')
            ->assertStatus(500)
            ->assertJsonFragment([
                'message' => 'Shopping cart is empty.',
            ]);
    }

    public function test_with_less_product_quantity_than_shopping_cart(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $product = Product::query()
            ->create([
                'name' => 'laptop',
                'description' => 'this is a laptop.',
                'quantity' => 1,
                'price' => 400,
            ]);

        CartItem::query()
            ->create([
                'user_id' => $user->id,
                'product_id' => $product->id,
                'quantity' => 2,
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/orders')
            ->assertStatus(500)
            ->assertJsonFragment([
                'message' => 'The product has less quantity than your request, please review your order.',
            ]);
    }

    public function test_store_order(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $product = Product::query()
            ->create([
                'name' => 'laptop',
                'description' => 'this is a laptop.',
                'quantity' => 10,
                'price' => 400,
            ]);

        CartItem::query()
            ->create([
                'user_id' => $user->id,
                'product_id' => $product->id,
                'quantity' => 2,
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/orders')
            ->assertCreated()
            ->assertJsonFragment([
                'status' => 'COMPLETED',
                'user_id' => $user->id,
            ]);

        $this->assertDatabaseHas('orders', [
            'user_id' => $user->id,
            'status' => 'COMPLETED',
            'discount_id' => null,
        ]);

        $this->assertDatabaseHas('order_items', [
            'product_id' => $product->id,
            'quantity' => 2,
        ]);

        $this->assertDatabaseHas('order_payments', [
            'amount' => 800,
            'status' => 'PAID',
        ]);

        $this->assertDatabaseCount('cart_items', 0);
    }

    public function test_store_order_with_discount(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $product = Product::query()
            ->create([
                'name' => 'laptop',
                'description' => 'this is a laptop.',
                'quantity' => 10,
                'price' => 400,
            ]);

        CartItem::query()
            ->create([
                'user_id' => $user->id,
                'product_id' => $product->id,
                'quantity' => 2,
            ]);

        $discount = Discount::query()
            ->create([
                'user_id' => $user->id,
                'code' => 'd28d2gdh2',
                'description' => 'This is for you',
                'amount' => 5,
                'is_active' => true,
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/orders', [
            'discount_code' => 'd28d2gdh2',
        ])
            ->assertCreated()
            ->assertJsonFragment([
                'status' => 'COMPLETED',
                'user_id' => $user->id,
            ]);

        $this->assertDatabaseHas('orders', [
            'user_id' => $user->id,
            'status' => 'COMPLETED',
            'discount_id' => $discount->id,
        ]);

        $this->assertDatabaseHas('order_items', [
            'product_id' => $product->id,
            'quantity' => 2,
        ]);

        $this->assertDatabaseHas('order_payments', [
            'amount' => 795,
            'status' => 'PAID',
        ]);

        $this->assertDatabaseHas('discounts', [
            'id' => $discount->id,
            'is_active' => false,
        ]);

        $this->assertDatabaseCount('cart_items', 0);
    }

    public function test_store_order_with_wrong_discount_code(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);
        $user1 = User::query()
            ->create([
                'email' => 'mohammad.mahabadi1@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593592',
                'name' => 'Mohammad',
            ]);

        $product = Product::query()
            ->create([
                'name' => 'laptop',
                'description' => 'this is a laptop.',
                'quantity' => 10,
                'price' => 400,
            ]);

        CartItem::query()
            ->create([
                'user_id' => $user->id,
                'product_id' => $product->id,
                'quantity' => 2,
            ]);

        Discount::query()
            ->create([
                'user_id' => $user->id,
                'code' => 'd28d2gdh2',
                'description' => 'This is for you',
                'amount' => 5,
                'is_active' => true,
            ]);

        Discount::query()
            ->create([
                'user_id' => $user1->id,
                'code' => 'd28d2gdh1',
                'description' => 'This is for you',
                'amount' => 5,
                'is_active' => true,
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/orders', [
            'discount_code' => 'd28d2gdh1',
        ])
            ->assertStatus(500)
            ->assertJsonFragment([
                'message' => 'The discount is not belong to you or it is not active.',
            ]);
    }
}
